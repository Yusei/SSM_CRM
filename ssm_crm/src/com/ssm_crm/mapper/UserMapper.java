package com.ssm_crm.mapper;

import java.util.List;

import com.ssm_crm.pojo.User;

/**
 * @author Yusei
 * 与用户相关的逻辑
 */
public interface UserMapper {
	// 用户登录
	public List<User> userLogin(User user);
	// 用户注册
	public void userRegist(User user);
	// 判断用户是否存在
	public String existUser(User user);
}
