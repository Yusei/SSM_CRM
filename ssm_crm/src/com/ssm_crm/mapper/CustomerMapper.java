package com.ssm_crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ssm_crm.pojo.Customer;
import com.ssm_crm.pojo.QueryVo;
import com.ssm_crm.pojo.SearchQueryVo;

/**
 * @author Yusei
 * 客户持久层接口声明
 */

public interface CustomerMapper {
	// 查询所有客户
	public List<Customer> findAll(QueryVo vo);
	// 根据id查询用户
	public Customer findCustomerById(String id);
	// 条件查询
	public List<Customer> search(SearchQueryVo svo);
	// 修改客户信息
	public void update(Customer customer);
	// 删除客户信息
	public void delete(Long id);
	// 查询客户列表的总记录数
	public Integer customerListCount();
	// 添加客户信息
	public void add(Customer customer);
	// 查询条件查询一共有多少条记录
	public Integer searchListCount(SearchQueryVo svo);
}
