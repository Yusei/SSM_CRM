package com.ssm_crm.mapper;

import java.util.List;

import com.ssm_crm.pojo.BaseDict;

/**
 * @author Yusei
 * 数据字典的dao接口文件
 */

public interface BaseDictMapper {
	// 根据id查询数据字典表
	public List<BaseDict> findByDictId(String id);
}
