package com.ssm_crm.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ssm_crm.pojo.Customer;
import com.ssm_crm.pojo.QueryVo;
import com.ssm_crm.pojo.SearchQueryVo;
import com.ssm_crm.service.CustomerService;

/**
 * @author Yusei
 * 客户控制层实现类
 * 
 */

@Controller
@RequestMapping("/customer/*")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService; 
	
	// 分页功能需要用到的参数
	// 当前页
	private Integer currPage;
	// 每页显示多少条数据
	private Integer pageSize;
	// 通过查询数据库获得一共有多少条数据
	private Integer count;
	// 总共有多少页（需要自己去算）
	private Integer total;
 	
	/**
	 * 测试用的方法
	 * @return
	 */
	@RequestMapping("helloSSM")
	public String helloSSM(){
		System.out.println("hello ssm");
		return "testList";
	}

	/**
	 * 打开客户列表页面
	 * @return
	 */
	@RequestMapping("list")
	public ModelAndView list(){
		ModelAndView mav = new ModelAndView("customer");
		return mav;
	}
	
	/**
	 * 打开修改客户信息界面
	 * @return
	 */
	@RequestMapping("edit")
	@ResponseBody
	public Customer edit(String id){
		// 根据id查询用户
		Customer customer = customerService.findCustomerById(id);
		return customer;
	}
	
	/**
	 * 条件查询（带分页）
	 */
	@RequestMapping("search")
	@ResponseBody
	public QueryVo search(@RequestBody SearchQueryVo svo){
		QueryVo vo = new QueryVo();
		// 默认显示第一页
		if (svo.getCurrPage() != null && svo.getCurrPage() != "") {
			this.currPage = Integer.valueOf(svo.getCurrPage());
		}else{
			svo.setCurrPage("1");
			this.currPage = 1;
		}
		// 查询条件查询一共有多少条记录
		count = customerService.searchListCount(svo);
		// 如果记录数为0，直接返回
		if(count == 0){
			return vo;
		}
		// 默认1页显示10条数据（页面大小等于10）
		this.pageSize = 10;
		// 计算一共有多少页（向上取整）
		Double total = Math.ceil(count.doubleValue() / this.pageSize);
		// 先用Double类型调用向上取整的方法，获得总页数，再转成Integer类型
		Integer totalPage = Integer.valueOf(total.intValue());
		// 防止越界
		if (svo.getCurrPage().equals("0")) {
			svo.setCurrPage("1");
			this.currPage = 1;
		}
		if (this.currPage > totalPage) {
			this.currPage = totalPage;
			svo.setCurrPage(totalPage.toString());
		}
		// 当前是第几页
		vo.setCurrPage(this.currPage);
		// 一页显示多少条数据
		vo.setPageSize(this.pageSize);
		// 从第几条开始查
		vo.setStartIndex((this.currPage - 1) * this.pageSize);
		// 设置返回的json数据
		svo.setStartIndex(vo.getStartIndex());
		svo.setPageSize(vo.getPageSize());
		// 调用Service层，进行条件查询
		List<Customer> list = customerService.search(svo);
		// 一共有多少页
		vo.setMaxPage(totalPage);
		// 封装数据，以json形式返回
		vo.setList(list);
		return vo;
	}
	
	/**
	 * 修改用户信息
	 */
	@RequestMapping("update")
	@ResponseBody
	public String update(@RequestBody Customer customer){
		String result = customerService.update(customer);
		return result;
	}

	/**
	 * 删除客户信息
	 */
	@RequestMapping("delete")
	@ResponseBody
	public String delete(Long id){
		String result = customerService.delete(id);
		return result;
	}
	
	/**
	 * 新增用户信息
	 */
	@RequestMapping("add")
	@ResponseBody
	public String add(@RequestBody Customer customer){
		// 服务器设置创建日期
		customer.setCust_createtime(new Date());
		// 调用service
		String result = customerService.add(customer);
		return result;
	}
	
}
