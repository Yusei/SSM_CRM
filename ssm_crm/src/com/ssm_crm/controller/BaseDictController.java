package com.ssm_crm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ssm_crm.pojo.BaseDict;
import com.ssm_crm.pojo.BaseDictList;
import com.ssm_crm.service.BaseDictService;

/**
 * @author Yusei
 * 数据字典的controller类
 */

@Controller
public class BaseDictController {
	
	@Autowired
	private BaseDictService baseDictService;
	
	@RequestMapping("getBaseDictList")
	@ResponseBody
	public BaseDictList getBaseDictList(){
		// 查询
		BaseDictList list = new BaseDictList();
		List<BaseDict>  cust_source_list = baseDictService.findByDictId("002");
		List<BaseDict>  cust_industry_list = baseDictService.findByDictId("001");
		List<BaseDict>  cust_level_list = baseDictService.findByDictId("006");
		
		// 包装数据
		list.setCust_industry_list(cust_industry_list);
		list.setCust_level_list(cust_level_list);
		list.setCust_source_list(cust_source_list);
		
		// 返回数据
		return list;
	}
	
}



