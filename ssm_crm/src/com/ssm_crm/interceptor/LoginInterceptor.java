package com.ssm_crm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Yusei
 * 登录拦截器
 * 如果只是单纯的想要实现登录拦截功能，只需要实现preHandle就可以了
 * 
 */
public class LoginInterceptor implements HandlerInterceptor{

	/**
	 * 在controller方法执行之后执行
	 * 一般用于日志记录，处理异常
	 */
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		
	}

	/**
	 * 在controller方法执行之后，返回ModelAndView之前执行
	 * 一般用于设置或者清理页面共用参数等等
	 */
	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		
	}

	/**
	 * 在controller方法执行前执行
	 * 常用于登录拦截，权限校验等等
	 * 返回true放行，返回false拦截
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
		// 从session中获取user
		Object user = request.getSession().getAttribute("user");
		// 如果user不为空，放行
		if (user != null) {
			return true;
		}else{
			// 重定向到登录页面
			response.sendRedirect(request.getContextPath() + "/login");
			// 拦截
			return false;
		}
	
	}

}
