package com.ssm_crm.pojo;

import java.util.List;

/**
 * @author Yusei
 * 用于返回数据字典结果的pojo
 */

public class BaseDictList {
	private List<BaseDict> cust_source_list;			// 客户信息来源
	private List<BaseDict> cust_industry_list; 		// 客户所属行业
	private List<BaseDict> cust_level_list;			// 客户级别
	
	public List<BaseDict> getCust_source_list() {
		return cust_source_list;
	}
	public void setCust_source_list(List<BaseDict> cust_source_list) {
		this.cust_source_list = cust_source_list;
	}
	public List<BaseDict> getCust_industry_list() {
		return cust_industry_list;
	}
	public void setCust_industry_list(List<BaseDict> cust_industry_list) {
		this.cust_industry_list = cust_industry_list;
	}
	public List<BaseDict> getCust_level_list() {
		return cust_level_list;
	}
	public void setCust_level_list(List<BaseDict> cust_level_list) {
		this.cust_level_list = cust_level_list;
	}
	
	
}


