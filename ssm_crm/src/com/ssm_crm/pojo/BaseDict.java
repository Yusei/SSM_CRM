package com.ssm_crm.pojo;


/**
 * @author Yusei
 * 数据字典的pojo
 */

public class BaseDict {
	private String dict_id;				// 数据字典id(主键)
	private String dict_item_name;		// 数据字典项目名称
	
	public String getDict_id() {
		return dict_id;
	}
	public void setDict_id(String dict_id) {
		this.dict_id = dict_id;
	}
	public String getDict_item_name() {
		return dict_item_name;
	}
	public void setDict_item_name(String dict_item_name) {
		this.dict_item_name = dict_item_name;
	}
	
	
	
}
