package com.ssm_crm.pojo;

import java.util.List;

/**
 * @author Yusei
 * 客户关系包装类
 */

public class QueryVo {
	// 当前页
	private Integer currPage;
	// 每页显示的数据
	private Integer pageSize;
	// 总共有多少页
	private Integer maxPage;
	// 从第几条开始查
	private Integer startIndex;
	// 客户列表
	private List<Customer> list;
	
	public Integer getCurrPage() {
		return currPage;
	}
	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}
	public Integer getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}
	public List<Customer> getList() {
		return list;
	}
	public void setList(List<Customer> list) {
		this.list = list;
	}

	
}
