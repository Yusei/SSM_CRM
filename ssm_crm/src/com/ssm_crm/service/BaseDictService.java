package com.ssm_crm.service;

import java.util.List;

import com.ssm_crm.pojo.BaseDict;

/**
 * @author Yusei
 * 数据字典的Service接口
 */


public interface BaseDictService {
	// 根据id查询数据字典
	public List<BaseDict> findByDictId(String id);
}
