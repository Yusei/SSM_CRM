package com.ssm_crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssm_crm.mapper.CustomerMapper;
import com.ssm_crm.pojo.Customer;
import com.ssm_crm.pojo.QueryVo;
import com.ssm_crm.pojo.SearchQueryVo;
import com.ssm_crm.service.CustomerService;

/**
 * @author Yusei
 * CRM系统的业务逻辑层的实现类
 */

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerMapper customerMapper;
	
	@Override
	// 测试用的方法
	public void print() {
		
	}

	@Override
	// 查询所有用户
	public List<Customer> findAll(QueryVo vo) {
		return customerMapper.findAll(vo);
	}

	// 根据id查询用户
	@Override
	public Customer findCustomerById(String id) {
		return customerMapper.findCustomerById(id);
	}

	@Override
	// 条件查询
	public List<Customer> search(SearchQueryVo svo) {
		return customerMapper.search(svo);
	}

	@Override
	// 修改客户信息
	public String update(Customer customer) {
		try{
			customerMapper.update(customer);
		}catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
		return "0";
	}

	@Override
	// 删除客户信息
	public String delete(Long id) {
		try{
			customerMapper.delete(id);
		}catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
		return "0";
	}

	@Override
	// 查询客户列表的总记录数
	public Integer customerListCount() {
		return customerMapper.customerListCount();
	}

	@Override
	// 添加客户信息
	public String add(Customer customer) {
		try{
			customerMapper.add(customer);
		}catch (Exception e) {
			e.printStackTrace();
			return "1";
		}
		return "0";

	}

	@Override
	// 查询条件查询一共有多少条记录
	public Integer searchListCount(SearchQueryVo svo) {
		return customerMapper.searchListCount(svo);
	}

}
