package com.ssm_crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssm_crm.mapper.BaseDictMapper;
import com.ssm_crm.pojo.BaseDict;
import com.ssm_crm.service.BaseDictService;

@Service
public class BaseDictServiceImpl implements BaseDictService{

	@Autowired
	private BaseDictMapper baseDictMapper;

	@Override
	public List<BaseDict> findByDictId(String id) {
		return baseDictMapper.findByDictId(id);
	}

}
