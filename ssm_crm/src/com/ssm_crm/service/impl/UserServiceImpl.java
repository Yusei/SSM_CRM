package com.ssm_crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ssm_crm.mapper.UserMapper;
import com.ssm_crm.pojo.User;
import com.ssm_crm.service.UserService;
import com.ssm_crm.utils.MD5Utils;

/**
 * @author Yusei
 * UserService接口的实现类
 */
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	// 用户登录
	@Override
	public User userLogin(User user) {
		// 对密码进行加密
		String password = MD5Utils.md5(user.getUser_password());
		user.setUser_password(password);
		// 调用dao层
		List<User> list = userMapper.userLogin(user);
		// 判断是否查询到结果
		if (list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	// 用户注册
	@Override
	public void userRegist(User user) {
		userMapper.userRegist(user);
		
	}

	// 判断用户是否存在
	@Override
	public String existUser(User user) {
		String result = userMapper.existUser(user);
		return result;
	}
	

}
