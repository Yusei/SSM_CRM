package com.ssm_crm.service;

import com.ssm_crm.pojo.User;

/**
 * @author Yusei
 * 用户登录Service接口
 */
public interface UserService {
	// 用户登录
	public User userLogin(User user);
	// 用户注册
	public void userRegist(User user);
	// 判断用户是否存在
	public String existUser(User user);
}
