package com.ssm_crm.service;

import java.util.List;

import com.ssm_crm.pojo.Customer;
import com.ssm_crm.pojo.QueryVo;
import com.ssm_crm.pojo.SearchQueryVo;

/**
 * @author Yusei
 * CRM系统的业务逻辑层
 */

public interface CustomerService {
	// 测试用的方法
	public void print();
	// 查询所有客户
	public List<Customer> findAll(QueryVo vo);
	// 根据id查询用户
	public Customer findCustomerById(String id);
	// 条件查询
	public List<Customer> search(SearchQueryVo svo);
	// 修改客户信息
	public String update(Customer customer);
	// 删除客户信息
	public String delete(Long id);
	// 查询客户列表的总记录数
	public Integer customerListCount();
	// 添加客户信息
	public String add(Customer customer);
	// 查询条件查询一共有多少条记录
	public Integer searchListCount(SearchQueryVo svo);
}
